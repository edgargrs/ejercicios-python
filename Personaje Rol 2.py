# -*- coding: utf-8 -*-

class Personaje:

    def __init__(self, pro, fue, arm, res, ene):
        self.proyectiles = pro
        self.fuerza = fue
        self.armas = arm
        self.resistencia = res
        self.energia = ene
        print "Se a creado el personaje"

    def atacar(self, accion):
        if(accion == 1):
            print ("Atacaste a golpes reduce tu energia en 2")
            self.energia -= 2
            self.fuerza -= 2
        elif(accion == 2):
            print ("Atacaste con proyectiles reduce tu energia en 1")
            self.energia -= 1
            self.proyectiles -= 1
        else:
            print ("Atacaste con la espada, reduce tu energia en 4")
            self.energia -= 4
            self.fuerza -= 4
        if(self.armas > 10):
                print ("Has subido a nivel 1")
        self.armas += 1

    def defender(self):
        self.resistencia -= 10
        if(self.resistencia <= 10):
            self.energia -= 10
            if(self.energia <= 0):
                print ("Has sido derrotado!!!")

    def tomarEnergia(self, energia):
        if(energia == 1):
            self.energia += 10
        elif(energia == 2):
            self.energia += 20
        elif(energia == 3):
            self.energia += 15

    def mostrar(self):
        print (("Resistencia: " + str(self.resistencia)))
        print (("Energia: " + str(self.energia)))
        print (("Fuerza: " + str(self.fuerza)))
        print ("\n")

TheKing = Personaje(50, 50, 8, 50, 50)

def menu():
    print("Selecciona una opción \n1. Ver atributos \n2. Atacar \n3. Defender \n4. Tomar energia \n5. Salir")

while True:
    menu()
    opcion = int(raw_input("¿Qué opción deseas? "))
    if(opcion == 1):
        print("\n")
        TheKing.mostrar()
    elif(opcion == 2):
        print("\n")
        TheKing.atacar(int(raw_input("¿De qué manera deseas atacar?\n 1.Golpes \n2.Proyectiles \n3.Espada ")))
    elif(opcion == 3):
        print("\n")
        TheKing.defender()
    elif(opcion == 4):
        print ("\n")
        TheKing.tomarEnergia(int(raw_input("¿Qué deseas comer? \n1. Pescado \n2 Pollo \n3. Res")))
    elif(opcion == 5):
        break
    else:
        print("\n")
        raw_input("No haz pulsado ninguna tecla correcta")
    TheKing.mostrar()


