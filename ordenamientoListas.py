# -*- coding: utf-8 -*-
nombres = ["Jose", "Jose", "Ricky", "Jacinto", "David", "Alvaro", "Ricky"]
#  nombres.sort()  # La ordena alfabeticamente
#  nombres.reverse()  # Acomoda de reversa la lista
#  nombres.sort(reverse=True)
#  print nombres.count("Jose")
#  print nombres.index("Jacinto")

# Concatenación de Listas
lista1 = [1, 2, 3, 4]
lista2 = [3, 4, 5, 6, 7]
lista3 = [lista1 + lista2]
#  print lista3

#  minimos y máximos
#  print max(lista2)
#  print min(lista2)

#  Contar elementos
#  print len(nombres[0])
#  print len(nombres)
