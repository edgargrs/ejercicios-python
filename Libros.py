# -*- coding: utf-8 -*-
import mysql.connector

con = mysql.connector.connect(user='root', password='1234', host="localhost",
    database="biblioteca")
cursor = con.cursor()


def muestraLibros():
    cursor.execute("SELECT * from libros")
    resultado = cursor.fetchall()
    for registro in resultado:
        print ((str(registro[0]), str(registro[1]), str(registro[2])))


def muestraEditoriales():
    cursor.execute("SELECT * from editoriales")
    resultado = cursor.fetchall()
    for registro in resultado:
        print ((str(registro[0]), str(registro[1])))


def insertalibros(titulo, autor, codigo, precio, cantidad):
    sql = ("INSERT INTO libros "
               "(titulo, autor, codigoEditorial, precio, cantidad) "
               "VALUES (%s, %s, %s, %s, %s)")
    try:
        param = (titulo, autor, codigo, precio, cantidad)
        cursor.execute(sql, param)
        con.commit()
        print("Libro creado")
    except:
        con.rollback()
        print("Error al crear el libro")


def actualizalibros(ide, titulo, autor, codigo, precio, cantidad):
    sql = ("UPDATE libros SET titulo=%s, autor=%s, codigoEditorial=%s, "
        "precio=%s, cantidad=%s WHERE codigo=%s")
    param = (titulo, autor, codigo, precio, cantidad, ide)
    try:
        cursor.execute(sql, param)
        con.commit()
        print("Actualizado correctamente")
    except:
        con.rollback()
        print("Error al actualizar el libro")


def eliminaLibros(ide):
    try:
        cursor.execute("DELETE FROM libros WHERE codigo= %s", [ide])
        con.commit()
        print("Eliminado correctamente")
    except:
        con.rollback()
        print("Error al eliminar el libro")


def menu():
    print("Selecciona una opción \n 1. Consultar libros \n 2. Crear libros\n "
    "3. Actualizar \n 4. Eliminar \n 5. Salir")


while True:
    menu()
    opcion = int(raw_input("¿Qué opción deseas? "))
    if(opcion == 1):
        print("Consultar")
        muestraLibros()
    elif(opcion == 2):
        print("Crear")
        nombre = raw_input("Nombre del Libro: ")
        autor = raw_input("Autor del Libro: ")
        muestraEditoriales()
        editorial = raw_input("Codigo de la editorial: ")
        precio = float(raw_input("Precio: "))
        cantidad = int(raw_input("Cantidad: "))
        print ("Nombre: " + nombre + ", Autor: " + autor + ", Editorial: " +
        editorial + ", Precio: " + str(precio) + ", Cantidad: " + str(cantidad))
        insertalibros(nombre, autor, editorial, precio, cantidad)
    elif(opcion == 3):
        print("Actualizar")
        muestraLibros()
        ide = raw_input("Id del libro a actualizar")
        nombre = raw_input("Nombre del Libro: ")
        autor = raw_input("Autor del Libro: ")
        muestraEditoriales()
        editorial = raw_input("Codigo de la editorial: ")
        precio = float(raw_input("Precio: "))
        cantidad = int(raw_input("Cantidad: "))
        print ("Nombre: " + nombre + ", Autor: " + autor + ", Editorial: " +
        editorial + ", Precio: " + str(precio) + ", Cantidad: " + str(cantidad))
        actualizalibros(ide, nombre, autor, editorial, precio, cantidad)
    elif(opcion == 4):
        print("Eliminar")
        muestraLibros()
        ide = raw_input("Id del libro a eliminar: ")
        eliminaLibros(ide)
    elif(opcion == 5):
        print("Salir")
        break
    else:
        print("No seleccionaste una opcion valida")