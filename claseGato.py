# -*- coding: utf-8 -*-


class Gato:
    def __init__(self, energia, hambre):
        self.energia = energia
        self.hambre = hambre
        print ("Se creó un gato")

    def tomarLeche(self, lecheLitros):
        self.hambre += lecheLitros
        print ("El gato toma su leche")

    def acariciar(self):
        print ("prrrr...")

    def jugar(self):
        if self.energia <= 0 or self.hambre <= 1:
            print ("No tengo energia no quiero jugar")
        else:
            self.energia -= 1
            self.hambre -= 2
            print ("Al gato le encanta jugar")

    def dormir(self, horas):
        self.energia += horas
        print ("El gato está durmiendo")

gatito = Gato(0, 0)
horas = int(raw_input("Cuanto a dormido tu gato: "))
gatito.dormir(horas)

gatito.tomarLeche(4)
gatito.acariciar()
gatito.jugar()
gatito.dormir(2)
