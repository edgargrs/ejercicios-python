# -*- coding: utf-8 -*-
#  Verificando Rectangulo Latino

"""  Un rectángulo latino es una cuadrícula de R renglones y C columnas donde
cada cuadrito tiene un entero del 1 al R+C y además todos los enteros de
cada renglón son distintos y todos los enteros de cada columna son
distintos. Tu tarea es verificar si una matriz corresponde con un
rectángulo latino.
Para ello deberás decir si cada renglón y columna están bien. En el
ejemplo mostrado abajo, los primeros dos renglones están mal ya que tienen
enteros repetidos y el tercer renglón está mal ya que tiene enteros fuera
de rango.

Entrada: Dos enteros R y C seguidos de una matriz de enteros con R renglones y
C columnas. Puedes suponer que 1 ≤ R ≤ 100, 1 ≤ C ≤ 100 y que las entradas de
la matriz son enteros entre 1 y 1000.

    Ejemplo de entrada
        3 4
        3 1 2 3
        1 5 6 5
        4 9 5 8

Salida: Un renglón con R enteros, uno por cada renglón de la matriz (0 si el
renglón está mal, 1 si el renglón está bien) seguido de un renglón con
C enteros, uno por cada columna de la matriz (0 si la columna está mal, 1
si la columna está bien).

      Ejemplo de salida
        0 0 0
        1 0 1 0
"""


class Clase:
    def __init__(obj, f, c, mt):
        obj.f = f
        obj.c = c
        obj.mt = mt
        obj.l = f + c

    def verifica(obj, temp, valor):
        if(len(temp) == 0):
            return False
        else:
            for i in range(len(temp)):
                if(valor == temp[i]):
                        return True
                else:
                        return False

    def fi(obj):
        for i in range(obj.f):
            for j in range(obj.c):
                temp = []
                if(obj.mt[i][j] <= obj.l):
                    temp.append(obj.mt[i][j])
                    if(not obj.verifica(temp, obj.mt[i][j])):
                        if(j == obj.c):
                            print ("1")
                            temp = []
                    else:
                        print ("0")
                        temp = []
                        break
                else:
                        print ("0")
                        temp = []
                        break


mat = [[3, 1, 2, 3], [1, 5, 6, 5], [4, 9, 5, 8]]
""" f = int(raw_input("Filas: "))
c = int(raw_input("Columnas: "))
mat = []
for i in range(f):
    mat.append([])
    for j in range(c):
        mat[i].append(int(raw_input("Fila :" + str(i + 1) + ",Columna: "
        + str(j + 1) + ": "))) """

vrl = Clase(3, 4, mat)
vrl.fi()