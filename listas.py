# -*- coding: utf-8 -*-
lista = [22, True, "Una lista", [1, 2]]

lis = ["a", "b", "mpilgrim", "z", "example"]
#print lis[-1]

#agregar elemento
#lis.append("new") #inserta al final
#lis.insert(2,"new") #inserta en posición dada
#lis.extend(["two", "elements"]) #inserta al final
#lis.remove("a") #elimina elemento
#print lis

#operadores de lista
lista2 = ["a", "b", "mpilgrim"]
lista2 = lista2 + ["example", "new"]
lista2 += ["three"]
lista3 = [1, 2] * 3
print lista2
print lista3