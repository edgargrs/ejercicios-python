class PersonajeRol:

    def __init__(self, proyectiles, fuerza, armas, resistencia, energia):
        self.proyec = proyectiles
        self.force = fuerza
        self.arms = armas
        self.resistance = resistencia
        self.energy = energia
        print "Se a creado el personaje"

    def atacar(self, accion):
        if(accion == "golpear"):
            if(self.arms > 10):
                print("Has subido a nivel 1")
        self.arms += 1

    def defender(self):
        self.resistance = self.resistance - 10
        if(self.resistance <= 0):
            self.energy = self.energy - 10
            if(self.energy <= 0):
                print ("Has sido derrotado")

    def tomarEnergia(self, comida):
        if(comida == "Pollo"):
            self.energy += 10
        elif(comida == "Res"):
            self.energy += 20
        elif(comida == "Pescado"):
            self.energy += 15

    def verAtributos(self):
        print("Armas llevas un progreso de: " + str(self.arms))
        print("Resistencia llevas un progreso de: " + str(self.resistance))
        print("Energia llevas un progreso de: " + str(self.energy))

personaje = PersonajeRol(0, 0, 1, 50, 50)


def menu():
    print ("Selecciona una opcion")
    print ("\t1 - Ver atributos")
    print ("\t2 - Atacar")
    print ("\t3 - Defender")
    print ("\t4 - Tomar energia")
    print ("\t9 - Salir")


while True:
    menu()
    opcionMenu = raw_input("inserta un numero valor >> ")
    if opcionMenu == "1":
        print ("")
        personaje.verAtributos()
    elif opcionMenu == "2":
        print ("")
        personaje.atacar("golpear")
    elif opcionMenu == "3":
        print ("")
        personaje.defender()
    elif opcionMenu == "4":
        print ("")
        comida = raw_input("Que comera, Pescado, Pollo o Res")
        personaje.tomarEnergia(comida)
    elif opcionMenu == "9":
        break
    else:
        print ("")
        raw_input("No has pulsado ninguna opcion correcta...")